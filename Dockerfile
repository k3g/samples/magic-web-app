FROM node:12.0-slim as build
WORKDIR /build
COPY *.json /build/
COPY *.js /build/
RUN npm install
RUN npm build

FROM node:lts-alpine
EXPOSE 8080/tcp
WORKDIR /app
COPY --from=build /build /app

CMD [ "npm", "start" ]
